# Rick and Morty Challenge!!

Junto con saludar, les comento de que trata el challenge y las características de la solución

# Challenge
Tienes que consultar los `character`, `locations` y `episodes` de [https://rickandmortyapi.com/](https://rickandmortyapi.com/) e indicar:
1. Char counter:
    - cuántas veces aparece la letra **"l"** (case insensitive) en los nombres de todos los `location`
    - cuántas veces aparece la letra **"e"** (case insensitive) en los nombres de todos los `episode`
    - cuántas veces aparece la letra **"c"** (case insensitive) en los nombres de todos los `character`
    - cuánto tardó el programa 👆 en total
2. Episode locations:
    - para cada `episode`, indicar la cantidad y un listado con las `location` (`origin`) de todos los `character` que aparecieron en ese `episode` (sin repetir)
    - cuánto tardó el programa 👆 en total

# Solución
- La Solución fue trabajada en React (fue una entretenida experiencia)
- Me faltó la última parte (indicar las `location`), pero les hago entrega de todo el resto
- Se rescataron datos con hooks y el código se encuentra documentado para su fácil lectura
- Se crearon 3 ramas para separar los ambientes en Develop (develop), Test (test) y Producción (master)
- Se encuentra además el .gitlab-ci.yml pero al no estar apuntando a ningún servidor no realiza el deploy. De todas formas adjunto la solución desde Netlify (https://tender-golick-218522.netlify.app/)