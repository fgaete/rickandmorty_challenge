import React,{useState,useEffect} from 'react';
const Characters = () => {
    //States
    const [character, setCharacter] = useState([])

    //Se realiza el fetch y los datos se setean en la constante previamente declarada
    useEffect(() => {
        fetch('https://rickandmortyapi.com/api/character')
            .then(response => response.json())
            .then(data => setCharacter(data.results))
    }, [])
    
    //Se recorre Array con los datos obtenidos y se guarda en variable char_names con el fin de posteriormente recorrer y contar
    var char_names = character.map(function (lista) {
 
        return lista.name; 
     
    });
    //Se setean variables y se inicia recorrido de Array char_names y se busca match de letra indicada en el challenge y se carga contador
    var conteo = 0;
    var letra = "c";
    for(var i = 0; i < char_names.length; i++) {
        for (var j=0; j<char_names[i].length;j++){
            if (char_names[i][j].toLowerCase() == letra ) conteo++;
        }
    }

    //Se retornan y dibujan los datos por pantalla
    return ( 
      <div className="container">
           <h2>La cantidad de "c" (case insensitive) en los nombres de los personajes es: <b className="resultado">{conteo}</b></h2>
      </div>
     );
}
 
export default Characters;