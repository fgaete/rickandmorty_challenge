import React from 'react';

const Header = ({title}) => {
    return ( 
        <header>
            <div className="container">
                <div className="content">
                    <h1 className="title">
                        Rick and Morty Challenge
                    </h1>
                </div>    
            </div>
        </header>
     );
}
 
export default Header;