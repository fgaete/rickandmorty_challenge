import React,{useState,useEffect} from 'react';
const ListCharxEpisode = () => {
    //States
    const [episode, setEpisode] = useState([])

    //Se realiza el fetch y los datos se setean en la constante previamente declarada
    useEffect(() => {
        fetch('https://rickandmortyapi.com/api/episode')
            .then(response => response.json())
            .then(data => setEpisode(data.results))
    }, [])
    
    //Se retornan y dibujan los datos por pantalla
    return ( 
      <div className="container">
          <div className="characters">
              {
                  episode.map( item=> (
                      <h2>El episodio <b>"{item.name}"</b> contiene una cantidad de {item.characters.length} personajes y se encuentran en : <li></li></h2>
                  ))
              }
          </div>
      </div>
     );
}
 
export default ListCharxEpisode;