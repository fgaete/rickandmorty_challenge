import React,{useState,useEffect} from 'react';

const Locations = () => {
    //States
    const [location, setLocation] = useState([])

    //Se realiza el fetch y los datos se setean en la constante previamente declarada
    useEffect(() => {
        fetch('https://rickandmortyapi.com/api/location')
            .then(response => response.json())
            .then(data => setLocation(data.results))
    }, [])

    //Se recorre Array con los datos obtenidos y se guarda en variable loc_names con el fin de posteriormente recorrer y contar
    var loc_names = location.map(function (lista) {
 
        return lista.name; 
     
    });

    //Se setean variables y se inicia recorrido de Array loc_names y se busca match de letra indicada en el challenge y se carga contador
    var conteo = 0;
    var letra = "l";
    for(var i = 0; i < loc_names.length; i++) {
        for (var j=0; j<loc_names[i].length;j++){
            if (loc_names[i][j].toLowerCase() == letra ) conteo++;
        }
    }

    //Se retornan y dibujan los datos por pantalla
    return ( 
      <div className="container">
           <h2>La cantidad de "l" (case insensitive) en los nombres de las localizaciones es: <b className="resultado">{conteo}</b></h2>
      </div>
     );
}
 
export default Locations;