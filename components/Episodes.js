import React,{useState,useEffect} from 'react';

const Episodes = () => {
    //States
    const [episode, setEpisode] = useState([])

    //Se realiza el fetch y los datos se setean en la constante previamente declarada
    useEffect(() => {
        fetch('https://rickandmortyapi.com/api/episode')
            .then(response => response.json())
            .then(data => setEpisode(data.results))
    }, [])

    //Se recorre Array con los datos obtenidos y se guarda en variable epi_names con el fin de posteriormente recorrer y contar
    var epi_names = episode.map(function (lista) {
 
        return lista.name; 
     
    });

    //Se setean variables y se inicia recorrido de Array epi_names y se busca match de letra indicada en el challenge y se carga contador
    var conteo = 0;
    var letra = "e";
    for(var i = 0; i < epi_names.length; i++) {
        for (var j=0; j<epi_names[i].length;j++){
            if (epi_names[i][j].toLowerCase() == letra ) conteo++;
        }
    }

    //Se retornan y dibujan los datos por pantalla
    return ( 
      <div className="container">
           <h2>La cantidad de "e" (case insensitive) en los nombres de los episodios es: <b className="resultado">{conteo}</b></h2>
      </div>
     );
}
 
export default Episodes;