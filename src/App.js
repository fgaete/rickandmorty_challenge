import React,{Fragment} from 'react'
import Characters from '../components/Characters'
import Episodes from '../components/Episodes'
import Locations from '../components/Locations'
import Header from '../components/Header'
import ListCharxEpisode from '../components/ListCharxEpisode'
import './main.sass'

const App = () => {
    return (  
        <Fragment>
            <Header />
            <h1>Ejercicio 1</h1>
            <Characters />
            <Episodes />
            <Locations />
            <h1>Ejercicio 2</h1>
            <ListCharxEpisode />
        </Fragment>
    );
    
}
export default App;